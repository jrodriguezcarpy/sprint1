//VAR VARIABLE DE FUNCION. Vale en todo el codigo. Si se las declara en una funcion vale solo en la funcion
//LET VARIABLE DE BLOQUE. Vale en todo el codigo. Si se las declara en una funcion o bloque vale solo en esa funcion o bloque
//CONST VARIABLE DE BLOQUE (NO SE PUEDE MODIFICAR SU VALOR)
function saludar(){//definicion de funcion
    console.log('hola funcion');
    //return undefined implicito
}
saludar();//llamada de funcion

let saludar2 = function(){//igual a lo anterior
    console.log('hola funcion2');
    //return undefined implicito
}
saludar2();

let saludar3 = () =>{//funcion FLECHA
    console.log('hola funcion3');
    //return undefined implicito
}
saludar3();

function suma(num1, num2, num3) {//funcion con parametros
    console.log(num1 + num2);
}
suma(2,4);

let suma2 = function(num1, num2){//igual a lo de arriba
    console.log(num1 + num2);
}
suma2(4,6);

let suma3 = (num1, num2) => {//funcion flecha
    const resultado = num1 + num2;
    imprimirResultado(resultado);//se puede crear una funcion dentro de otra y esa segundo funcion vive solo dentro de la primera
    function imprimirResultado(result){
        console.log(result);
    }
}
suma3(1,4);

let suma4 = num1 => console.log(num1*2);//funcion flecha cuando tiene una sola linea
suma4(4);

function click(){
    console.log('click en boton');
}
document.getElementById('btn').addEventListener('click', click);//callback! llamamos a una funcion como parametro de otra funcion
document.getElementById('btn2').addEventListener('click', () =>{//callback! con funcion flecha
    console.log('click en boton2');
});