document.getElementById('cuadrado').addEventListener('click', () =>{
    let numero = document.getElementById('numero').value;
    let cuadrado = num => num**2;
    document.getElementById('resultado').innerText = `${cuadrado(numero)}`;
});
document.getElementById('factorial').addEventListener('click', () =>{
    let numero = document.getElementById('numero').value;
    let fact = numero;
    function factorial(num) {
        for (i = num-1; i > 1; i--){
            fact = fact * i;
        }
        return fact;
    }
    document.getElementById('resultado').innerHTML = `${factorial(numero)}`;
});
document.getElementById('area').addEventListener('click', () =>{
    let pi = Math.PI;
    let radio = document.getElementById('numero').value;
    function area (radio){
        return pi * (radio**2);
    };
    document.getElementById('resultado').innerHTML = `${area(radio)}`;
});
document.getElementById('login').addEventListener('click', () =>{
    let usuarios = [
        ['juan', '123'],
        ['maxima', '234'],
        ['mara', '345'],
        ['cesar', '456']
    ]
    let usuario = document.getElementById('usuario').value;
    let pass = document.getElementById('pass').value;
    function inicioSesion (user, contrasenia){
        for (i=0; i<usuarios.length; i++){
            if (user === usuarios[i][0] && contrasenia === usuarios[i][1]){
                return true;
            }
        }
        return false
    }
    document.getElementById('resultado2').innerHTML = `${inicioSesion(usuario,pass)}`;
});