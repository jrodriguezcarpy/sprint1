const coolImages = require ('cool-images');
const fs = require('fs');
if (fs.existsSync('images.txt')) {
    fs.unlink('images.txt', (err) => {
        if (err) throw err;
      });
}
const imagenUno = coolImages.one();
console.log(imagenUno);
const imagenes = coolImages.many(600,800,10);
// console.log(imagenes);
imagenes.forEach((imagen, indice) =>{
    console.log(`Imagen ${indice + 1}: ${imagen}`);
    fs.appendFileSync('images.txt', `URL${indice + 1}: ${imagen}\n`, (err) => {
        if (err) throw err;
      });
})