const moment = require('moment');

const ahora = moment(new Date());
console.log(ahora.format('MMMM Do YYYY, h:mm:ss a'));
const ahorautc = moment.utc();
console.log(ahorautc.format('MMMM Do YYYY, h:mm:ss a'));
const horaahora = ahora.hour();
// console.log(horaahora);
const horaahorautc = ahorautc.hour();
// console.log(horaahorautc);
const diferenciahoraria = horaahora - horaahorautc;
console.log(diferenciahoraria);
const antes = ahorautc.isBefore(ahora);
console.log(antes);