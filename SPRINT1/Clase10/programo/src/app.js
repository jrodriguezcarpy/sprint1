const env = require('./appsettings.json');
//console.log(env.dev);
require('dotenv').config();

console.log('desde dotenv', process.env.NODE_ENV);

const node_env = process.env.NODE_ENV || 'dev';
const variables = env[node_env];
console.log(node_env);
console.log(variables);
//TEORIA
//EN APPSETTINS.JSON SE PONEN LAS 'KEYS' DE CADA AMBIENTE CON LOS VALORES QUE CORRESPONDEN A CADA UNO
//URL PARA NPM: https://www.npmjs.com/


//PASOS (mi app.js se crea en una carpeta SRC dentro de donde vamos a trabajar)
//1)
    //ABRO MI FOLDER EN CONSOLA Y LE DIGO QUE VAMOS A TRABAJAR CON NPM -> npm init (si quiero ahorrar todos los datos npm init -y)
    //CON EL PASO DE ARRIBA SE CREA EL ARCHIVO 'package.json'
        //AQUI DENTRO EN 'SCRIPTS' SE PUEDE LLAMAR A LAS EJECUCIONES (START, DEV, ETC) PARA CORRERLO npm start. LOS DEMAS npm run 'NOMBRE QUE TENGA ESA EJECUCION.
//2) (si trabajo con ambientes)
    //DENTRO DE LA CARPETA SRC CREO EL ARCHIVO 'appsettings.json'
    //COLOCO LAS 'KEYS' DE CADA AMBIENTE CON LOS VALORES QUE CORRESPONDEN A CADA UNO
//3) (si trabajo con ambientes)
    //PARA TRAER LAS VARIABLES DE ENTORNO en app.js:
        //const env = require('./appsettings.json');
        // const node_env = process.env.NODE_ENV || 'dev';
        // const variables = env[node_env];
        // console.log(node_env);(ESTO SOLO IMPRIME PARA VALIDAR QUE TRAJO)
        // console.log(variables);(ESTO SOLO IMPRIME PARA VALIDAR QUE TRAJO)
//4) (si trabajo con ambientes)
    //A) TENEMOS QUE INSTALAR LA LIBRERIA DE npm
        //a) la buscamos en https://www.npmjs.com/
        //b) comando: npm install dotenv (a nivel de la carpeta de trabajo. OSEA EN EL MISMO NIVEL DE SRC y PACKAGE.JSON)
        //c) INSTALADO
    //B) CREAR ARCHIVO
        //a) Crear archivo.env (en el mismo nivel que SRC y PACKAGE.JSON) -> DEFINE QUE VARIABLES SE VAN A USAR!
        //b) Colocar NODE_ENV = 'ambiente que se haya definido en appsettings.json' (ej: dev, qa, etc)
    //C)
        //a) Hay que traer la libreria en app.js con REQUIRE
        //b) Linea: require('dotenv').config();
        //c) esto lo que hace es que la linea de app.js 'process.env.NODE_ENV' trae lo que se define en el .env
//5)
    //SI BORRO (o no traigo conmigo) LA CARPETA DE 'node_modules' NO FUNCIONA EL CODIGO. PERO SI ESTA CONTEMPLADO EN PACKAGE.JSON ENTONCES CORRIENDO npm install SE INSTALAN LAS LIBRERIAS NECESARIAS PARA QUE FUNCIONE NUESTRO CODIGO
//6)
    //.gitignore -> SE COLOCAN LOS NOMBRES DE LO QUE NO QUEREMOS QUE SUBA AL REPOSITORIO (EN EL MISMO NIVEL DE SRC y PACKAGE.JSON)
    //http://gitignore.io/ ayuda a generar los gitignore
//7)
    //INSTALAR NODEMON!
        //npm install nodemon -D (para instalarlo como desarrollador y no como dependencia del proyecto)
        //para usarlo DEBERIA ser 'nodemon 'y la ruta al archivo' PERO ME DA ERROR. En su lugar lo ejecuto con nodemon desde package.json como se describe en el punto 1 arriba de todo (en los scripts) -> npm run 'el script que corresponda'
//8)
    //INSTALAR LIBRERIAS!
        //Posicionarse en la carpeta. Buscar en NPM el comando para instalar y chau.