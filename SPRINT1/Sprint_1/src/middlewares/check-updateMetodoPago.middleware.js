const Metodo = require('../Controler/metodopago.controler');

function checkMetodoUpdate (req, res, next){
    const indexMetodo = Metodo.getAllMetodosPago().findIndex(m => m.metodo === req.body.metodo);
    const index = Metodo.getAllMetodosPago().findIndex(m => m.id === req.params.id)
    // console.log(`Metodo: ${indexMetodo}`);
    // console.log(`ID: ${index}`);
    if (!req.body.metodo){
        res.json('Campo de metodo vacio');
    }else if(indexMetodo >= 0){
        res.json('Metodo de pago ya existente');
    }else if(index >= 0){
        res.json('No existe metodo de pago con el ID enviado')
    }
    else{
        next();
    };
};

module.exports = checkMetodoUpdate;