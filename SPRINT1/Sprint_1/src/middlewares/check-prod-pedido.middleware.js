const express = require('express');
const Producto = require('../models/producto.model');
function ValidacionProducto (req, res, next){
    const { items } = req.body;
    let total = 0;
    let prod = 0;
    let cancelarPedido = false;
    let prodfallido = 0;
    if (Array.isArray(items)){
        for (let index = 0; index < items.length; index++) {
            prod = Producto.getAllProductos().findIndex(p => p.id === items[index].id);
            if (prod === -1){
                cancelarPedido = true;
                prodfallido = index;
            }else{
                total = total + (parseInt(items[index].cantidad) * parseInt(Producto.getAllProductos()[prod].precio));
                items[index].producto = Producto.getAllProductos()[prod].producto;
                items[index].precio = Producto.getAllProductos()[prod].precio;
                // console.log(`item ${parseInt(req.body.items[index][1])}`);
                // console.log(`prod ${parseInt(Producto.getAllProductos.precio)}`);
                // console.log(total);
            };
        }
    }
    // console.log(total);
    if (cancelarPedido === false){
        req.body.total = total;
        next();
    }else{
        res.json(`Producto ${items[prodfallido].id} no existe`);
    }
};

module.exports = ValidacionProducto;