const Metodo = require('../Controler/metodopago.controler');

function checkMetodoDelete (req, res, next){
    const { id } = req.params
    const index = Metodo.getAllMetodosPago().findIndex(m => m.id === parseInt(id))
    if (index === -1){
        res.json('No existe metodo de pago con el ID enviado');
    }else{
        next();
    };
};

module.exports = checkMetodoDelete;