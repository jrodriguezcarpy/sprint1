const Producto = require('../models/producto.model');

function checkProd (req, res, next){
    if (!req.body.producto || !req.body.precio){
        res.json('Informacion proporcionada insuficiente para la carga de un nuevo producto.');
    }else{
        const index = Producto.getAllProductos().findIndex(p => p.producto === req.body.producto)
        if (index === -1){
            next();
        }else{
            res.json('Producto ingresado ya existente.')
        }
    }
};

module.exports = checkProd;