const basicAuth = require('express-basic-auth');
const User = require('../models/user.model');

function myCustomAuthorizer (username, password){
    const users = User.getAllUsers().filter(u => u.username === username);

    if(users.length <= 0) return false;

    const userMatches = basicAuth.safeCompare(username, users[0].username);
    const passwordMatches = basicAuth.safeCompare(password, users[0].password);

    return userMatches & passwordMatches;
};

module.exports = myCustomAuthorizer;