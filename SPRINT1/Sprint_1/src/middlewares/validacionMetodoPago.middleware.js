const MetodoPago = require('../Controler/metodopago.controler');

function ValidacionMetodoPago (req, res, next){
    const { metodoPago } = req.body;
    const index = MetodoPago.getAllMetodosPago().findIndex(m => m.id === parseInt(metodoPago))
    if (index === -1){
        res.json('No existe metodo de pago seleccionado');
    }else{
        next();
    }
}

module.exports = ValidacionMetodoPago;