const express = require('express');
const User = require('../models/user.model');

function validacionCreacion (req, res, next){
    const { username, nombre, apellido, email, password } = req.body;
    const filtro = User.getAllUsers().filter(u => u.email === email);
    if (!username || !nombre || !apellido || !email || !password){
        res.json('Campo/s obligatorio/s vacios.');
    }else if (filtro.length !== 0){
        res.json('Correo ya utilizado en un usuario preexistente.')
    }else{
        next();
    }
}

module.exports = validacionCreacion;