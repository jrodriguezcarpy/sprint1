const express = require('express');
const User = require('../models/user.model');
let index = 0;

function isAdmin (req, res, next){
    index = User.getAllUsers().findIndex(u => u.username === req.auth.user)
    if (User.getAllUsers()[index].admin === true){
        next();
    }else{
        res.json(User.getAllUsers()[index].username + ' NO ES ADMIN');        
    }
};

module.exports = isAdmin;