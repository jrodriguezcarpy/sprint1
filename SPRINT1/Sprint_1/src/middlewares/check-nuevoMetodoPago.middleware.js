const Metodo = require('../Controler/metodopago.controler');

function checkMetodo (req, res, next){
    const index = Metodo.getAllMetodosPago().findIndex(m => m.metodo === req.body.metodo);
    if (!req.body.metodo){
        res.json('Campo de metodo vacio');
    }else if(index >= 0){
        res.json('Metodo de pago ya existente');
    }else{
        next();
    };
};

module.exports = checkMetodo;