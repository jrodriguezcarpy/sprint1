const { MetodoPago } = require('../models/metodopago.model');

const getAllMetodosPago = () =>{
    return MetodoPago;
}

const createNewMetodoPago = (req, res)=>{
    console.log(req.body);
    const metodo = req.body;
    console.log(metodo);
    metodo.id = getAllMetodosPago().length + 1;
    console.log(metodo);
    MetodoPago.push(metodo);
    res.status(200).send(metodo);
};

const updateMetodoPago = (req, res)=>{
    const { id } = req.params;
    // console.log(id);
    const index = MetodoPago.findIndex(metodo => metodo.id === parseInt(id))
    // console.log(index);
    if (index >= 0){
        MetodoPago[index].metodo = req.body.metodo;
        // console.log(MetodoPago[index]);
    }
    res.status(201).send(MetodoPago[index]);
}

const deleteMetodoPago = (req, res)=> {
    const { id } = req.params;
    const index = MetodoPago.findIndex(metodo => metodo.id === parseInt(id));
    // console.log(index);
    //agregar else si no encuentra id
    if (index >= 0){
        MetodoPago.splice(index, 1);
        // console.log(MetodoPago[index]);
    }
    res.status(204).send();
};

module.exports = {getAllMetodosPago, createNewMetodoPago, updateMetodoPago, deleteMetodoPago};