const express = require('express');
const router = express.Router();
const {getAllMetodosPago, createNewMetodoPago, updateMetodoPago, deleteMetodoPago} = require('../Controler/metodopago.controler');
const isAdmin = require('../middlewares/is-admin.middleware');
const checkMetodo = require('../middlewares/check-nuevoMetodoPago.middleware');
const checkMetodoUpdate = require('../middlewares/check-updateMetodoPago.middleware');
const checkMetodoDelete = require('../middlewares/check-deleteMetodoPago.middleware');

/**
 * @swagger
 * /metodospago/todos:
 *  get:
 *      summary: Devuelve todos los metodos de pago
 *      tags: [MetodosdePago]
 *      responses:
 *          200:
 *              description: Lista de metodos de pago
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/MetododePago'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/todos', isAdmin, (req, res) =>{
    res.json(getAllMetodosPago());
});

/**
 * @swagger
 * /metodospago/crearmetodopago:
 *  post:
 *      summary: Metodo de pago nuevo
 *      tags: [MetodosdePago]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/MetododePagoNuevo'
 *      responses:
 *          200:
 *              description: Metodo de pago creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/MetododePago'
 *          401:
 *              description: Sin autorizacion
 */
router.post('/crearmetodopago', isAdmin, checkMetodo, createNewMetodoPago);

/**
 * @swagger
 * /metodospago/editarmetodopago/{id}:
 *  put:
 *      summary: Actualiza un metodo de pago en particular
 *      tags: [MetodosdePago]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: ID del metodo de pago
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/MetododePagoNuevo'
 *                  example:
 *                      metodo: Pagare
 *      responses:
 *          201:
 *              description: Resultado
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/MetododePago'
 *          401:
 *              description: Sin autorizacion
 */
router.put('/editarmetodopago/:id', isAdmin, checkMetodoUpdate,updateMetodoPago);

/**
 * @swagger
 * /metodospago/borrarmetodopago/{id}:
 *  delete:
 *      summary: Elimina un metodo de pago en particular
 *      tags: [MetodosdePago]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: ID del metodo de pago a eliminar
 *      responses:
 *          204:
 *              description: Metodo de pago eliminado eliminado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Sin autorizacion
 */
router.delete('/borrarmetodopago/:id', isAdmin, checkMetodoDelete, deleteMetodoPago);

/**
 * @swagger
 * tags:
 *  name: MetodosdePago
 *  description: Seccion de metodos de pago
 * 
 * components:
 *  schemas:
 *      MetododePago:
 *          type: object
 *          properties:
 *              id:
 *                  type: number
 *                  description: ID de metodo de pago
 *              metodo:
 *                  type: string
 *                  description: Nombre del metodo de pago
 *          example:
 *              id: 1
 *              metodo: Efectivo
 *      MetododePagoNuevo:
 *          type: object
 *          required:
 *              - metodo
 *          properties:
 *              metodo:
 *                  type: string
 *                  description: Nombre de nuevo metodo de pago
 *          example:
 *              metodo: Cheque
 */
module.exports = router;