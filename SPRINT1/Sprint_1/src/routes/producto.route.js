const express = require('express');
const router = express.Router();
const Product = require('../models/producto.model');
const isAdmin = require('../middlewares/is-admin.middleware');
const checkNuevoProd = require('../middlewares/check-prod-nuevo.middleware');

/**
 * @swagger
 * /productos:
 *  get:
 *      summary: Devuelve todos los productos
 *      tags: [Productos]
 *      responses:
 *          200:
 *              description: Lista de productos
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Producto'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/', (req, res) =>{
    res.json(Product.getAllProductos());
});
/**
 * @swagger
 * /productos/nuevoproducto:
 *  post:
 *      summary: Producto nuevo
 *      tags: [Productos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Productonuevo'
 *      responses:
 *          200:
 *              description: Producto creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Sin autorizacion
 */
router.post('/nuevoproducto', isAdmin, checkNuevoProd, (req, res) =>{
    req.body.id = Product.getAllProductos()[Product.getAllProductos().length - 1].id + 1;
    Product.createNewProducto(req.body);
    res.json('Producto creado');
});
/**
 * @swagger
 * /productos/{idproducto}:
 *  put:
 *      summary: Actualiza un producto en particular
 *      tags: [Productos]
 *      parameters:
 *          -   in: path
 *              name: idproducto
 *              schema:
 *                  type: number
 *              required: true
 *              description: ID del producto
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Productonuevo'
 *                  example:
 *                      Producto: Coca
 *                      Precio: 100
 *      responses:
 *          200:
 *              description: Resultado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Sin autorizacion
 */
router.put('/:idproducto', isAdmin, (req, res) =>{
    const { idproducto } = req.params;
    const index = Product.getAllProductos().findIndex(p => p.id == idproducto);
    if (index === -1){
        res.json(`No existe el producto ID: ${idproducto}`);
    }else if (!req.body.producto || !req.body.precio){
        res.json('Nuevo/s campo/s incompleto/s');
    }else {
        Product.getAllProductos()[index].producto = req.body.producto;
        Product.getAllProductos()[index].precio = req.body.precio;
        res.json(`Producto ID ${idproducto} actualizado`);
    };
});
/**
 * @swagger
 * /productos/{idproducto}:
 *  delete:
 *      summary: Elimina un producto en particular
 *      tags: [Productos]
 *      parameters:
 *          -   in: path
 *              name: idproducto
 *              schema:
 *                  type: number
 *              required: true
 *              description: ID del producto
 *      responses:
 *          200:
 *              description: Producto eliminado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Sin autorizacion
 */
router.delete('/:idproducto', isAdmin, (req, res) =>{
    const { idproducto } = req.params;
    const index = Product.getAllProductos().findIndex(p => p.id == idproducto);
    if (index === -1){
        res.json(`No existe el producto ID: ${idproducto}`);
    }else {
        Product.getAllProductos().splice(index, 1);
        res.json(`Producto ID ${idproducto} eliminado`);
    };
});

/**
 * @swagger
 * tags:
 *  name: Productos
 *  description: Seccion de productos
 * 
 * components:
 *  schemas:
 *      Producto:
 *          type: object
 *          required:
 *              - producto
 *              - precio
 *          properties:
 *              id:
 *                  type: number
 *                  description: ID de producto
 *              producto:
 *                  type: string
 *                  description: Nombre del producto
 *              precio:
 *                  type: number
 *                  description: Precio del producto
 *          example:
 *              id: 1
 *              producto: Bagel
 *              precio: 425
 *      Productonuevo:
 *          type: object
 *          required:
 *              - producto
 *              - precio
 *          properties:
 *              producto:
 *                  type: string
 *                  description: Nombre del producto
 *              precio:
 *                  type: number
 *                  description: Precio del producto
 *          example:
 *              producto: Pizza
 *              precio: 300
 */

module.exports = router;