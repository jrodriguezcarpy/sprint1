const express = require('express');
const router = express.Router();
const Pedido = require('../models/pedido.model');
const User = require('../models/user.model');
const ValidacionProducto = require('../middlewares/check-prod-pedido.middleware');
const isAdmin = require('../middlewares/is-admin.middleware');
const ValidacionMetodoPago = require('../middlewares/validacionMetodoPago.middleware');
// const validacionCreacion = require('../middlewares/validacion-creacion-usuario.middleware');
// const app = express();
// app.use(express.json());

/**
 * @swagger
 * /pedidos/todos:
 *  get:
 *      summary: Registro de todos los pedidos
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de pedidos de todos los usuarios
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Pedido'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/todos', isAdmin, (req, res) =>{
    res.json(Pedido.getAllPedidos());
});
/**
 * @swagger
 * /pedidos/mispedidos:
 *  get:
 *      summary: Registro de pedidos del usuario
 *      tags: [Pedidos]
 *      responses:
 *          200:
 *              description: Lista de pedidos del usuario
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/Pedido'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/mispedidos', (req, res) =>{
    const pedidos = Pedido.getAllPedidos().filter(p => p.usuario === req.auth.user);
    res.json(pedidos);
});
/**
 * @swagger
 * /pedidos/hacerpedido:
 *  post:
 *      summary: Pedido nuevo
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/PedidoRealizado'
 *      responses:
 *          200:
 *              description: Pedido creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Pedido'
 *          401:
 *              description: Sin autorizacion
 */
router.post('/hacerpedido', ValidacionProducto, ValidacionMetodoPago, (req, res) =>{
    req.body.numero = Pedido.getAllPedidos().length + 1;
    req.body.usuario = req.auth.user;
    req.body.direccion = User.getAllUsers().filter(u => u.username === req.body.usuario)[0].direccion;
    req.body.estado = 'Pendiente';
    Pedido.agregarItem(req.body);
    res.json(req.body);
});
/**
 * @swagger
 * /pedidos/cambioestado:
 *  put:
 *      summary: Cambio de estado de pedidos (Admins)
 *      tags: [Pedidos]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/CambioEstadoPedido'
 *      responses:
 *          200:
 *              description: Estado de pedido actualizado
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Pedido'
 *          401:
 *              description: Sin autorizacion
 */
router.put('/cambioestado', isAdmin, (req, res) =>{
    const index = Pedido.getAllPedidos().findIndex(p => p.numero === req.body.numero);
    if (index === -1){
        res.json(`No existe pedido numero: ${req.body.numero}`);
    }else{
        Pedido.getAllPedidos()[index].estado = req.body.estado;
        res.json(`Pedido numero ${req.body.numero} ACTUALIZADO`);
    }
    // Pedido.updatePedido(req.body);
});
router.delete('/', (req, res) =>{
    res.json('Delete response');
});

/**
 * @swagger
 * tags:
 *  name: Pedidos
 *  description: Seccion de pedidos
 * 
 * components:
 *  schemas:
 *      PedidoRealizado:
 *          type: object
 *          required:
 *              - items
 *              - metodoPago
 *          properties:
 *              items:
 *                  type: array
 *                  items:
 *                      type: object
 *                      required:
 *                          - id
 *                          - cantidad
 *                      properties:
 *                          id:
 *                              type: number
 *                          cantidad:
 *                              type: number
 *              metodoPago:
 *                  type: number
 *                  description: ID del metodo de pago
 *          example:
 *              {
 *                  items:
 *                      [
 *                          {
 *                              id: 2,
 *                              cantidad: 3
 *                          },
 *                          {
 *                              id: 1,
 *                              cantidad: 2
 *                          }
 *                      ],
 *                  metodoPago: 1
 *              }
 *      Pedido:
 *          type: object
 *          properties:
 *              usuario:
 *                  type: string
 *                  description: Usuario
 *              numero:
 *                  type: string
 *                  description: Numero de pedido
 *              items:
 *                  type: array
 *                  items:
 *                      type: object
 *                      properties:
 *                          id:
 *                              type: number
 *                          producto:
 *                              type: string
 *                          precio:
 *                              type: number
 *                          cantidad:
 *                              type: number
 *              total:
 *                  type: number
 *                  description: Total del pedido
 *              metodoPago:
 *                  type: number
 *                  description: ID Metodo de pago
 *              direccion:
 *                  type: string
 *                  description: Direccion del pedido
 *              estado:
 *                  type: string
 *                  description: Estado del pedido
 *          example:
 *              usuario: user
 *              numero: 1
 *              items:
 *                  [
 *                      {
 *                          id: 1,
 *                          producto: ejemplo,
 *                          precio: 100,
 *                          cantidad: 5,
 *                      }
 *                  ]
 *              total: 1125
 *              metodoPago: 1
 *              direccion: aaaaaaa
 *              estado: Pendiente
 *      CambioEstadoPedido:
 *          type: object
 *          required:
 *              - numero
 *              - estado
 *          properties:
 *              numero:
 *                  type: integer
 *                  description: Numero de pedido
 *              estado:
 *                  type: string
 *                  description: Nuevo estado del pedido
 *          example:
 *              numero: 1
 *              estado: Cerrado
 */

module.exports = router;