const express = require('express');
// const validacionCreacion = require('../middlewares/validacion-creacion-usuario.middleware');
const router = express.Router();
const User = require('../models/user.model');
const auth = require('../middlewares/basic-auth.middleware');
const isAdmin = require('../middlewares/is-admin.middleware');

/**
 * @swagger
 * /users:
 *  get:
 *      summary: Return all users
 *      tags: [Users]
 *      responses:
 *          200:
 *              description: The list of users
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/User'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/', isAdmin, (req, res) =>{
    // console.log(req.auth);
    res.json(User.getAllUsers());
});

/**
 * @swagger
 * /users/{username}:
 *  get:
 *      summary: Get specific user by username
 *      tags: [Users]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: The user name
 *      responses:
 *          200:
 *              description: The list of users
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/User'
 *          401:
 *              description: Sin autorizacion
 */
router.get('/:username', (req, res) =>{//GET con parametros
    const {username} = req.params
    // console.log(req.auth);
    res.json(User.getAllUsers().filter(u => u.username === username)[0]);
});

/**
 * @swagger
 * /usuarionuevo:
 *  post:
 *      summary: Create new user
 *      tags: [Users]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: The user was created
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          401:
 *              description: Sin autorizacion
 */

// POST DE USUARIOS EN APP.JS ANTES DE LA AUTENTICACION
// router.post('/',validacionCreacion,(req, res) =>{
//     req.body.admin = false;
//     User.createNewUsers(req.body);
//     res.json('Usuario creado');
// });
router.put('/', auth, (req, res) =>{
    res.json('Put response');
});
router.delete('/', auth, (req, res) =>{
    res.json('Delete response');
});

/**
 * @swagger
 * tags:
 *  name: Users
 *  description: User section
 * 
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - username
 *              - password
 *              - email
 *              - direccion
 *          properties:
 *              username:
 *                  type: string
 *                  description: Username
 *              nombre:
 *                  type: string
 *                  description: User name
 *              apellido:
 *                  type: string
 *                  description: User lastname
 *              email:
 *                  type: string
 *                  description: User email
 *              telefono:
 *                  type: number
 *                  description: User phone number
 *              direccion:
 *                  type: string
 *                  description: User address
 *              password:
 *                  type: string
 *                  description: User password
 *              admin:
 *                  type: boolean
 *                  description: True is user is admin
 *          example:
 *              username: user2
 *              nombre: user2
 *              apellido: Gilada
 *              email: user2@sarasa.com
 *              telefono: 1151413121
 *              direccion: aaaaaaa
 *              password: pass
 *              admin: true
 */

module.exports = router;