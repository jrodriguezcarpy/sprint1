const basicAuth = require('express-basic-auth');
const express = require('express');
const swaggerJsDoc = require ('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const userRoutes = require('./routes/user.route');
const productRoutes = require('./routes/producto.route');
const pedidoRoutes = require('./routes/pedido.route');
const metodospagoRoutes = require('./routes/metodospago.route');
const myCustomAuthorizer = require('./middlewares/basic-auth.middleware');
const validacionCreacion = require ('./middlewares/validacion-creacion-usuario.middleware');
const User = require('./models/user.model');
const swaggerOptions = require ('./utils/swaggerOptions');

const app = express();
app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.post('/usuarionuevo', validacionCreacion, (req, res) =>{
    req.body.admin = false;
    User.createNewUsers(req.body);
    res.json('Usuario creado');
});
app.use(basicAuth({authorizer : myCustomAuthorizer}));
app.use('/users', userRoutes);
app.use('/productos', productRoutes);
app.use('/pedidos', pedidoRoutes);
app.use('/metodospago', metodospagoRoutes);

app.listen(3000, () =>{
    console.log('Server listening on PORT 3000');
});