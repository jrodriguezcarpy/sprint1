const MetodoPago = [
    {
        id: 1,
        metodo: 'Efectivo'
    },
    {
        id: 2,
        metodo: 'Tarjeta de credito'
    },
    {
        id: 3,
        metodo: 'Tarjeta de debito'
    }
];

module.exports = {MetodoPago};