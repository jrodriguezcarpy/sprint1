const users = [
    {
        username: 'user',
        nombre: 'Juan',
        apellido: 'Rodriguez',
        email: 'lachilavert@gmail.com',
        telefono: 1161972323,
        direccion: 'eeeeeee',
        password: 'pass',
        admin: false
    },
    {
        username: 'admin',
        nombre: 'Admin',
        apellido: 'Gilada',
        email: 'admin@sarasa.com',
        telefono: 1151413121,
        direccion: 'aaaaaaa',
        password: 'pass',
        admin: true
    }
];

const getAllUsers = ()=>{
    return users;
};

const createNewUsers = (user)=>{
    users.push(user);
};

module.exports = {getAllUsers, createNewUsers};