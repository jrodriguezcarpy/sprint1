const productos = [
    {
        id: 1,
        producto: 'Bagel',
        precio: 425
    },
    {
        id: 2,
        producto: 'Hamburguesa',
        precio: 350
    },
    {
        id: 3,
        producto: 'Sandwich Veggie',
        precio: 310
    },
    {
        id: 4,
        producto: 'Ensalada Veggie',
        precio: 340
    },
    {
        id: 5,
        producto: 'Focaccia',
        precio: 300
    },
    {
        id: 6,
        producto: 'Sandwich Focaccia',
        precio: 440
    }
];

const getAllProductos = () =>{
    return productos;
}

const createNewProducto = (producto)=>{
    productos.push(producto);
};


module.exports = {getAllProductos, createNewProducto};