class pedido{
    constructor(usuario, numero, items, total, metodoPago, direccion, estado){
        this.usuario = usuario;
        this.numero = numero;
        this.items = items;
        this.total = total;
        this.metodoPago = metodoPago;//pendiente
        this.direccion = direccion;
        this.estado = estado;
    };
};

const pedidos = [];

const getAllPedidos = () =>{
    return pedidos;
};

const agregarItem = (pedido)=>{
    pedidos.push(pedido);
};

const updatePedido = (pedido) =>{
    // const index = pedidos.findIndex(p => p.numero === pedido.numero);
    // if (index === -1){
    //     res.json(`No existe pedido numero: ${pedido.numero}`);
    // }else{
    //     pedidos[index].estado = pedido.estado;
    //     res.json(`Pedido numero ${pedido.numero} ACTUALIZADO`);
    // }
}

module.exports = {getAllPedidos, agregarItem, updatePedido};