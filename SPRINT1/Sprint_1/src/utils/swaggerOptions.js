const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Mi primera API",
            version: "1.0.0",
            description: "Delilah Resto API"
        },
        servers: [
            {
                url: 'http://localhost:3000',
                description: 'Local Server'
            },
            {
                url: 'http://development.com:3000',
                description: 'Dev Server'
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: 'http',
                    scheme: 'basic'
                }
            }
        },
        security: [
            {
                basicAuth: []
            }
        ]
    },
    apis: ["./routes/*.js"]
}

module.exports = swaggerOptions;