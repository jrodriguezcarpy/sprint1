Delilah Restó

Proyecto para el curso de desarrollo web back end de Acamica

Aqui encontrará las instrucciones para correr este proyecto de manera local

Installation
Delilah requiere Node.js v12+ to run.
Instalar las dependencias y correr el servidor.:
git clone https://gitlab.com/jrodriguezcarpy/sprint1/-/tree/master/SPRINT1/Sprint_1
cd delilah-resto
npm i
npm run start-dev
Para ingresar a la documentacion hecha en swagger:
http://localhost:3000/api-docs/
Usuarios de prueba
Admin
- username: admin
- password: pass
No Admin
- username: user
- password: pass

License
MIT