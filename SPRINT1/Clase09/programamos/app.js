const fs = require ('fs');//LLAMA LA LIBRERIA
const hobbies = ['Futbol', 'Programar', 'Tocar guitarra', 'Pintar', 'Leer'];//DECLARA ARRAY
hobbies.forEach(hobby =>{//RECORRE EL ARRAY
    console.log(hobby);//IMPRIME CADA VALOR
    fs.appendFile('archivo.txt', hobby + '\n', () =>{//CON APPEND CREA Y VA AGREGANDO AL FINAL EN EL archivo.txt CADA HOBBY
        console.log('Archivo guardado');//CALLBACK QUE SE EJECUTA CADA VEZ QUE PASA POR EL APPEND
    });
});