const mascotas = ['Gato', 'Perro', 'Loro', 'Hamster'];
mascotas.push('Tortuga');//AGREGANDO AL FINAL

const mascotasDescripcion = [
    {
        nombre: 'Perro',
        descripcion: 'Animal canino de compania',
        estimadoVidaL: 12,
    },
    {
        nombre: 'Gato',
        descripcion: 'Animal felino de compania',
        estimadoVidaL: 15
    },
    {
        nombre: 'Pajaro',
        descripcion: 'Ave de compania',
        estimadoVidaL: 5
    }
]

// console.log(mascotas);//MUESTRA EL ARRAY
// for (i=0; i<mascotas.length; i++){//RECORRE EL ARRAY ENTERO (FORMA VIEJA Y CHOTA)
//     const mascota = mascotas[i];
//     console.log(mascota);
// }

//FOR EACH
mascotas.forEach((mascota, indice) => {//RECORREMOS EL ARRAY DE UNA FORMA MAS CHETA
    console.log(mascota, indice);
});

//MAP (es lo mismo que for each pero asincrono (llamado a la base de datos al tiempo) y este devuelve un array)
mascotas.map((mascota, indice) => {//LO MISMO QUE LO DE ARRIBA PERO ASINCRONO ???? KEKARAJO (NO CREA ARRAY NUEVO)
    console.log(mascota, indice);
});
const resultado = mascotas.map((mascota) => {//ESTO VA A DEVOLVER UN ARRAY NUEVO!
    return 'A-' + mascota;
});
console.log(resultado);

//FILTER
const filtrado = mascotas.filter((mascota) => mascota.length>4);//CREA UN NUEVO ARRAY QUE CUMPLA CON LA CONDICION QUE SE LE PASO
console.log(filtrado);

//FIND INDEX
const index = mascotas.findIndex((mascota) => mascota === 'Perro');//DEVUELVE LA POSICION
console.log(index);

//SPLICE
mascotas.splice(3, 1);//PARA BORRAR. PRIMERO DONDE SE PARA Y DESPUES CUANTO PARA ADELANTE SE QUIERE BORRAR
console.log(mascotas);

//FIND (devuelve lo que haya en la posicion del array donde es TRUE lo que se pasa en el parentesis)
const resultadoObjeto = mascotasDescripcion.find(mascota => mascota.descripcion.length > 15);
console.log(resultadoObjeto);