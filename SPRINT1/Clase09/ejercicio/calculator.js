function sumar (num1, num2){
    return num1 + num2;
};
function restar (num1, num2){
    return num1 - num2;
}
function multiplicar (num1, num2){
    return num1 * num2
}
function dividir (num1, num2){
    if (num2 === 0){
        return ('No se puede dividir por CERO')
    }else{
        return num1 / num2;
    }
}
exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;