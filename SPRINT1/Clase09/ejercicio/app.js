const fs = require('fs');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const recursion = function(){
    rl.question('Ingresar texto (0 para finalizar)', (texto) =>{
        if (texto == '0'){
            return rl.close();
        }else{
            fs.appendFile('texto.txt', `${texto}\n`, (error) =>{
                if(error) return error;
            });
        };
        recursion();
    });
};
recursion();