const calculator = require('./calculator');
const fs = require('fs');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const recursion = function(){
    rl.question('--OPCIONES A EJECUTAR:\n1) Sumar\n2) Restar\n3) Dividir\n4) Multiplicar\n5) Salir\nOPCION ELEGIDA: ', (opc) =>{
        switch (opc){
            case '1':
                let a, b, resultadoSuma;
                rl.question('Ingrese numero: ', (num1) =>{
                    a = parseInt(num1);
                    rl.question('Ingrese otro numero: ', (num2) =>{
                        b = parseInt(num2);
                        resultadoSuma = calculator.sumar(a,b);
                        // console.log(`resultado: ${resultado}`);
                        fs.appendFile('resultado.txt', `${a} + ${b} = ${resultadoSuma}\n`, (error) =>{
                            if(error) return error;
                        });
                        recursion();
                    });
                });
                break;
            case '2':
                let c, d, resultadoResta;
                rl.question('Ingrese numero: ', (num1) =>{
                    c = parseInt(num1);
                    // console.log(`numero1: ${num1}`);
                    rl.question('Ingrese otro numero: ', (num2) =>{
                        d = parseInt(num2);
                        resultadoResta = calculator.restar(c,d);
                        // console.log(`resultado: ${resultado}`);
                        fs.appendFile('resultado.txt', `${c} - ${d} = ${resultadoResta}\n`, (error) =>{
                            if(error) return error;
                        });
                        recursion();
                    });
                });
                break;
            case '3':
                let e, f, resultadoDivision;
                rl.question('Ingrese numero: ', (num1) =>{
                    e = parseInt(num1);
                    // console.log(`numero1: ${num1}`);
                    rl.question('Ingrese otro numero: ', (num2) =>{
                        f = parseInt(num2);
                        resultadoDivision = calculator.dividir(e,f);
                        // console.log(`resultado: ${resultado}`);
                        fs.appendFile('resultado.txt', `${e} / ${f} = ${resultadoDivision}\n`, (error) =>{
                            if(error) return error;
                        });
                        recursion();
                    });
                });
                break;
            case '4':
                let g, h, resultadoMulti;
                rl.question('Ingrese numero: ', (num1) =>{
                    g = parseInt(num1);
                    // console.log(`numero1: ${num1}`);
                    rl.question('Ingrese otro numero: ', (num2) =>{
                        h = parseInt(num2);
                        resultadoMulti = calculator.multiplicar(g,h);
                        // console.log(`resultado: ${resultado}`);
                        fs.appendFile('resultado.txt', `${g} * ${h} = ${resultadoMulti}\n`, (error) =>{
                            if(error) return error;
                        });
                        recursion();
                    });
                });
                break;
            case '5':
                return rl.close();
                break;
            default:
                console.log('Opcion incorrecta');
        };
        recursion();
    });
};
recursion();