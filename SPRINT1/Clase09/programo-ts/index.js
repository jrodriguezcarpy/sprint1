//TS = JS PERO TS ES TIPADO. (no permite cambio de tipo de variable)
//POR CONSOLA CORRESR: tsc 'NombreDelArchivo.ts' -w (inicia watch , crea archivo js, y lo va actualizando. Ctrl + C para frenar el watch)
var nombres = ['Mauricio', 'Michael', 'James'];
nombres.push('Lionel');
console.log(nombres);
var variable; //se pueden declarar los tipos de variables para luego ser asignadas
variable = 'hola';
console.log(variable);
var arraystring = [];
arraystring.push('hola');
console.log(arraystring);
var arraymixto = [];
arraymixto.push(1);
arraymixto.push('dos');
console.log(arraymixto);
var mascotas_detalladas;
