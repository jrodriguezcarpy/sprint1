//TS = JS PERO TS ES TIPADO. (no permite cambio de tipo de variable)
//POR CONSOLA CORRESR: tsc 'NombreDelArchivo.ts' -w (inicia watch , crea archivo js, y lo va actualizando. Ctrl + C para frenar el watch)

let nombres = ['Mauricio', 'Michael', 'James'];
nombres.push('Lionel');
console.log(nombres);

let variable: string; //se pueden declarar los tipos de variables para luego ser asignadas
variable = 'hola';
console.log(variable);
let arraystring: string[] = [];
arraystring.push('hola');
console.log(arraystring);
let arraymixto: (number|string)[] = [];
arraymixto.push(1);
arraymixto.push('dos');
console.log(arraymixto);
let mascotas_detalladas:{
    nombre:string,
    descripcion:string,
    edad_max_promedio:number;
}
