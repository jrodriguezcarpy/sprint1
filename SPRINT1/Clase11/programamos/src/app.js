const express = require('express');
const app = express();

const telefonos = [
    {
        marca: 'Samsung',
        modelo: 'S11',
        gama: 'Alta',
        pantalla: '19:9',
        sistema_operativo: 'Android',
        precio: 1000
    },
    {
        marca: 'Iphone',
        modelo: '12 Pro',
        gama: 'Alta',
        pantalla: 'OLED',
        sistema_operativo: 'iOs',
        precio: 1500
    },
    {
        marca: 'Xiaomi',
        modelo: 'Note 10s',
        gama: 'Media',
        pantalla: 'OLED',
        sistema_operativo: 'Android',
        precio: 300
    },
    {
        marca: 'LG',
        modelo: 'LG el que sea',
        gama: 'Alta',
        pantalla: 'OLED',
        sistema_operativo: 'Android',
        precio: 800
    },
    {
        marca: 'Falopa',
        modelo: 'Falopa Berreta',
        gama: 'Baja',
        pantalla: 'B&N',
        sistema_operativo: 'Android',
        precio: 100
    }
];

app.get('/telefonos', (req, res) =>{
    res.json(telefonos);
});

//REVISAR ESTO. NO SIRVE PARA DEVOLVER LA MITAD PORQUE REEMPLAZA EL ARRAY ORIGINAL (para mi)
// app.get('/mitadTelefonos', (req, res) =>{
//     const telefonos2 = telefonos.splice(0, (telefonos.length / 2));
//     res.json(telefonos2);
// });

app.get('/menorValor', (req, res) =>{
    let menor = telefonos[0];
    let precioMenor = telefonos[0].precio;
    telefonos.forEach((telefono) =>{
        if (telefono.precio < precioMenor){
            precioMenor = telefono.precio;
            menor = telefono;
        };
    });
    res.json(menor);
});

app.get('/mayorValor', (req, res) =>{
    let mayor = telefonos[0];
    let precioMayor = telefonos[0].precio;
    telefonos.forEach((telefono) =>{
        if (telefono.precio > precioMayor){
            precioMayor = telefono.precio;
            mayor = telefono;
        };
    });
    res.json(mayor);
});

app.get('/agrupar', (req, res) =>{
    let gamaBaja = telefonos.filter((telefono) => telefono.gama === 'Baja');
    let gamaMedia = telefonos.filter((telefono) => telefono.gama === 'Media');
    let gamaAlta = telefonos.filter((telefono) => telefono.gama === 'Alta');
    res.json({
        gamaAlta,
        gamaMedia,
        gamaBaja
    });
});

app.listen(3000, () =>{
    console.log('Servidor escuchando puerto 3000');
});