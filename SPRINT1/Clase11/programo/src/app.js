const express = require('express');
const app = express();
require('dotenv').config();

const PORT = process.env.PORT || 5000;

const listaUsuarios = ['user1', 'user2', 'user3']

//GET A LA RAIZ DE SERVICIO WEB
// app.get('/', (req, res) =>{
//     res.send('Hola mundo!');
// });
app.use(express.static('./src/public'));

//CRUD DE USUARIOS
app.get('/usuarios',(req, res) =>{
    res.json(listaUsuarios);
});

app.post('/usuarios',(req, res) =>{
    res.json('Post de usuarios!');
});

app.put('/usuarios',(req, res) =>{
    res.json('Put de usuarios!');
});

app.delete('/usuarios',(req, res) =>{
    res.json('Delete de usuarios!');
});

//CRUD DE PRODUCTO
app.get('/productos',(req, res) =>{
    res.json('Get de productos');
});

app.post('/productos',(req, res) =>{
    res.json('Post de productos!');
});

app.put('/productos',(req, res) =>{
    res.json('Put de productos!');
});

app.delete('/productos',(req, res) =>{
    res.json('Delete de productos!');
});

app.listen(PORT, () =>{
    console.log('Escuchando en el puerto PORT');
});