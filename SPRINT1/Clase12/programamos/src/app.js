const express = require('express');
const app = express();
app.use(express.json());

const usuarios = [
    { id: 1, nombre: 'Pepe', email: 'pepe@nada.com' },
    { id: 2, nombre: 'Hugo', email: 'hugo@nada.com' },
    { id: 3, nombre: 'Juan', email: 'juan@nada.com' }
];

app.use((req, res, next) => {
    console.log(req.url);
    next();
});

app.get('/usuario/:id', (req, res) => {
    const { id } = req.body;
    const users = usuarios.filter(u => u.id == id);
    if (users[0]) {
        res.json(users[0]);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No se encontro usuario"
            }
        );
    }
});

app.post('/usuario', (req, res) => {
    const { busqueda } = req.body;
    const users = usuarios.filter(u => u.nombre == busqueda);
    if (users[0]) {
        res.json(users[0]);
    } else {
        res.status(404).json(
            {
                status: false,
                msg: "No se encontro usuario"
            }
        );
    }
})

app.listen(3000, () => { console.log('Escuchando en el puerto 3000'); })