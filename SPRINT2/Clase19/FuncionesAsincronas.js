/**
 * Funciones asíncronas
 */

// ----------------------------------------------------------------
// Función sincrónica 1

// function f() {
//     let function_name = "f";
//     console.log(`From ${function_name}`);
// }

// const f = function() {
//     let function_name = "f";
//     console.log(`From ${function_name}`);
// }

// const f = () => {
//     let function_name = "f";
//     console.log(`From ${function_name}`);
// }

// ----------------------------------------------------------------
// Función sincrónica 2

// function f() {
//     let function_name = "f";
//     console.log(`From ${function_name}`);
// }

// function g() {
//     let function_name = "g";
//     console.log(`From ${function_name}`);
// }

// f();
// g();

// ----------------------------------------------------------------
// Función asincrónica 1

// function f() {
//     let function_name = "f";
//     let timeout = 2 * 1000;
//     setTimeout(
//         () => console.log(`From ${function_name}`),
//         timeout
//     )
// }

// function g() {
//     let function_name = "g";
//     console.log(`From ${function_name}`);
// }

// f();
// g();