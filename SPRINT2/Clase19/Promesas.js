/**
 * Promesas
 * States:
 *     - Fulfilled (resolved)
 *     - Rejected (rejected)
 *     - Pending (ongoing)
 */

// ----------------------------------------------------------------
// Promesas 1

// let p = new Promise((resolve, reject) => {
//     resolve("Promesa resuelta");
// });

// let q = new Promise((resolve, reject) => {
//     resolve("Promesa rechazada");
// });

// p.then((response) => {
//     console.log(response);
// }).catch(reason => {
//     console.log(reason);
// });

// q.then((response) => {
//     console.log(response);
// }).catch(reason => {
//     console.log(reason);
// });

// ----------------------------------------------------------------
// Promesas 2

// let p = new Promise((resolve, reject) => {
//     setTimeout(function(){
//         resolve("Promesa resuelta");
//     }, 250);
//     setTimeout(function(){
//         reject("Promesa rechazada");
//     }, 350);
// });

// p.then((response) => {
//     console.log(response);
// }).catch(reason => {
//     console.log(reason);
// });

// ----------------------------------------------------------------
// Promesas 3

// let p = new Promise((resolve, reject) => {
//     console.log("Antes");
//     resolve("Promesa resuelta");
//     console.log("Despues");
// });

// p.then((response) => {
//     console.log(response);
// }).catch(reason => {
//     console.log(reason);
// });

// ----------------------------------------------------------------
// Promesas 4

// let p = new Promise((resolve, reject) => {
//     console.log("Antes");
//     return resolve("Promesa resuelta");
//     console.log("Despues");
// });

// p.then((response) => {
//     console.log(response);
// }).catch(reason => {
//     console.log(reason);
// });