/**
 * 
 * PARA MANDAR A CATCH VALIDAR STATUS CODE -> 
 * if (response.status !== 200){
 *  throw new Error ('mensaje de error');
 * };
 * 
 * Fetch
 */

// npm i node-fetch
// const fetch = require("node-fetch");

// ----------------------------------------------------------------
// Fetch 1

// const url = "https://dog.ceo/api/breeds/image/random";

// fetch(url).then(response => {
//     response.json().then(
//         responseJSON => {
//             console.log(responseJSON);
//         }
//     );
// });

// ----------------------------------------------------------------
// Fetch 2

// const url = "https://dog.ceo/api/breeds/image/random";

// fetch(url)
//   .then(response => response.json())
//   .then(responseJSON => console.log(responseJSON))
//   .catch(reason => console.log(reason));

// ----------------------------------------------------------------
// Fetch 3

// const url = "https://dxog.ceo/api/breeds/image/random";

// function getImage() {
//     return fetch(url)
//         .then(response => response.json())
//         .then(responseJSON => responseJSON)
//         .catch(reason => reason);
// }

// getImage()
//   .then(responseJSON => {
//       console.log(`Se resolvió la promesa: ${responseJSON}`);
//   })
//   .catch(reason => console.log(`Se rechazó la promesa: ${reason}`));

// ----------------------------------------------------------------
// Fetch 4

// const url = "https://dog.ceo/api/breeds/image/random";

// function getImage() {
//     return fetch(url)
//         .then(response => {
//             console.log("Response");
//             console.dir(response);
//             return response.json();
//         })
//         .then(responseJSON => {
//             console.log(`Response JSON: ${JSON.stringify(responseJSON)}`);
//             return responseJSON;
//         })
//         .catch(reason => {
//             console.log(`Reason: ${reason}`);
//             return reason;
//         });
// }

// getImage()
//   .then(responseJSON => {
//       console.log(`Se resolvió la promesa: ${responseJSON}`);
//   })
//   .catch(reason => console.log(`Se rechazó la promesa: ${reason}`));