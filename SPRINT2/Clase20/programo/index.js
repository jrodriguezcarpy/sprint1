const fetch = require('node-fetch');
const key = 'ff60497e';
const url = `https://www.omdbapi.com/?apikey=${key}&t=`;

async function getMovie(pelicula) {
    const response = await fetch(url+pelicula);
    const result = await response.json();
    return result;
}

getMovie('the post')
    .then(result => console.log(result));