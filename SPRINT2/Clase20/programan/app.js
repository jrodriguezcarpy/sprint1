const express = require('express')
​
const router = require('./routes/movies')
​
const app = express()
​
​
app.use(express.json())
​
app.use(router)
​
​
app.listen(3000, () => {
  console.log('Servidor escuchando en el puerto 3000')
})
​
​
/* GET localhost:{PORT}/movie?title={title}
GET localhost:{PORT}/movie/{title}
POST localhost:{PORT}/movie/
{
} */