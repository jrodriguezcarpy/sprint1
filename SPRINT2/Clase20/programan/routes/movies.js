const express = require('express')
const { default: fetch } = require('node-fetch')
​
const router = express.Router()
​
const apiKey = ""
​
​
router.get('/movie', async (req, res) => {
  const {title} = req.query
  const movie = await getMovieInfo(title)
​
  return res.status(200).send(movie)
})
​
router.get('/movie/:title', async (req, res)=> {
  const {title} = req.params
  const movie = await getMovieInfo(title)
​
  return res.status(200).send(movie)
})
​
router.post('/movie', async(req, res) => {
  const { title } = req.body
  const movie = await getMovieInfo(title)
​
  return res.status(200).send(movie)
})
​
async function getMovieInfo(title) {
  let url = `https://www.omdbapi.com/?apikey=${apiKey}&t=${title}`
  try {
    const movieData = await fetch(url)
    return await movieData.json()
  } catch (error) {
    console.error(error)
  }
}
​
​
module.exports = router