/**
 * promise
 */

// ----------------------------------------------------------------
// all

// let p = new Promise((resolve, reject) => {
//     resolve("P resolved");
// });

// let q = new Promise((resolve, reject) => {
//     resolve("Q resolved");
// });

// let s = new Promise((resolve, reject) => {
//     reject("S rejected");
// });

// Promise.all([p, q, s]) //ARRAY DE PROMESAS
//     .then(response => { //ENTRA ACA SI TODAS LAS PROMESAS SE RESUELVEN
//         console.log(`Response: ${JSON.stringify(response)}`);
//     })
//     .catch(reason => { //CON QUE UNA SE RECHAZE VA AL CATCH
//         console.log(`Reason: ${reason}`);
//     });

// ----------------------------------------------------------------
// race //TOMA EL QUE TERMINE PRIMERO

// let p = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve("P resolved");
//     }, 500);
// });

// let q = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         resolve("Q resolved");
//     }, 300);
// });

// let s = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         reject("S rejected");
//     }, 600);
// });

// Promise.race([p, q, s])
//     .then(response => {
//         console.log(`Response: ${JSON.stringify(response)}`);
//     })
//     .catch(reason => {
//         console.log(`Reason: ${reason}`);
//     });