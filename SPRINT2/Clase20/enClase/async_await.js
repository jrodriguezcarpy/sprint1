/**
 * async/await
 */

//  const fetch = require("node-fetch");

 // ----------------------------------------------------------------
 // async/await 1
 
 /*
 function getInfoFromGithub(username) {
     const url = `https://api.github.com/users/${username}`;
     return fetch(url)
         .then(response => response.json())
         .then(response => response)
         .catch(reason => reason);
 }
 */
 
 // async function getInfoFromGithub(username) {
 //     const url = `https://api.github.com/users/${username}`;
     
 //     let response = await fetch(url);
 //     let responseJSON = await response.json();
 
 //     return responseJSON;
 // }
 
 // let info = getInfoFromGithub("000paradox000");
 // info
 //   .then(response => console.log(`Resolved: ${JSON.stringify(response)}`))
 //   .catch(reason => console.log(`Rejected: ${reason}`));
 
 // ----------------------------------------------------------------
 // async/await 2
 
 // async function getInfoFromGithub(username) {
 //     const url = `https://api.github.com/users/${username}`;
     
 //     let response = await fetch(url);
 //     let responseJSON = await response.json();
 
 //     return responseJSON;
 // }
 
 // (async function () {
 //     let info = await getInfoFromGithub("000paradox000");
 //     console.log(info);
 // })()